:set number
:set tabstop=4
:set shiftwidth=4
:set expandtab
:set autoindent
:set nocp
:set background=dark
call pathogen#infect()
syntax on
filetype plugin indent on
